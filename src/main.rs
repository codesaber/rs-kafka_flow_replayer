extern crate af_packet;
extern crate kafka;
extern crate libc;
extern crate nom;

use std::io::Error;
use std::process::Command;

fn main() {
    new_veth().unwrap();
    //let interface = af_packet::tx::
    let interface = af_packet::tx::Player::open_socket("veth0").unwrap();
    let mut kafka_client = kafka::client::KafkaClient::new(vec!["crads.io".to_string()]);
    for msg in kafka_client.fetch_messages_for_partition(&kafka::client::FetchPartition::new("crads", 1, 0)).into_iter() {
        //interface.send_frame();
    }
}

fn new_veth() -> Result<(),i32> {
    let status = Command::new("ip")
        .arg("link")
        .arg("add")
        .arg("veth0")
        .arg("type")
        .arg("veth")
        .arg("peer")
        .arg("name")
        .arg("veth1")
        .status()
        .expect("unable to execute iproute2");
    if !status.success() {
        println!("iproute2 failed to create virtual network");
        return Err(status.code().unwrap());
    }
    Ok(())
}
